import { Component, OnInit } from '@angular/core';
import { HeroService } from './../hero.service';
import { Hero } from './../hero';
import { MessageService } from './../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  };

  heroes: Hero[];

  selectedHero: Hero;

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    // clear the message when the first hero is chose
    if (this.messageService.messages[0] === 'HeroService: fetched heroes') {
      this.messageService.clear();
    }
    // show message when a hero is chose
    this.messageService.add('A hero is chose! ' + hero.name);
  }

  constructor(private heroService: HeroService, private messageService: MessageService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    // this.heroes = this.heroService.getHeroes();
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

}
